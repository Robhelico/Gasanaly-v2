import { Descriptions } from "antd";
import NumberFormat from "react-number-format";

const ShipmentDetails: React.FC<{ calculation: any }> = function (props) {
  const calculation = props.calculation;
  return (
    <div>
      <Descriptions
        title={calculation.calculation}
        labelStyle={{ fontWeight: "bold", backgroundColor: "rgb(199 232 255)" }}
        contentStyle={{ backgroundColor: "#e6f7ff" }}
        bordered
      >
        <Descriptions.Item label="Id" span={3}>
          {calculation.id}
        </Descriptions.Item>
        <Descriptions.Item label="Public Id" span={3}>
          {calculation.public_id}
        </Descriptions.Item>
        {calculation.mode && (
          <Descriptions.Item label="Mode">{calculation.mode}</Descriptions.Item>
        )}
        {calculation.type && (
          <Descriptions.Item label="Type">{calculation.type}</Descriptions.Item>
        )}
        <Descriptions.Item label="Weight" span={2}>
          <NumberFormat
            value={calculation.weight}
            displayType={"text"}
            thousandSeparator={" "}
            decimalScale={2}
          />{" "}
          kg
        </Descriptions.Item>
        <Descriptions.Item label="Distance">
          <NumberFormat
            value={calculation.distance}
            displayType={"text"}
            thousandSeparator={" "}
            decimalScale={2}
          />{" "}
          km
        </Descriptions.Item>
        {calculation.from_address && (
          <Descriptions.Item label="From">
            <a
              href={
                "https://maps.google.com/?q=" +
                calculation.from_location?.coordinates[1] +
                "," +
                calculation.from_location?.coordinates[0]
              }
              target="_blank"
            >
              {calculation.from_address}
            </a>
          </Descriptions.Item>
        )}
        {calculation.to_address && (
          <Descriptions.Item label="To">
            <a
              href={
                "https://maps.google.com/?q=" +
                calculation.to_location?.coordinates[1] +
                "," +
                calculation.to_location?.coordinates[0]
              }
              target="_blank"
            >
              {calculation.to_address}
            </a>
          </Descriptions.Item>
        )}
      </Descriptions>
    </div>
  );
};

export default ShipmentDetails;
