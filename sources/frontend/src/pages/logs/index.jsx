import { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { Alert, Table, Tag, Drawer, Typography } from "antd";
import moment from "moment";
import ShipmentDetails from "./shipmentDetails";
import DeliveryDetails from "./deliveryDetails";
import NumberFormat from "react-number-format";
import apiClient from "services/axios";
import { syntaxHighlight } from "utilityFunctions/display";

const { Text } = Typography;

const Logs = () => {
  //Filter and Sort
  const [paginationInfo, setPaginationInfo] = useState({
    current: 1,
    pageSize: 50,
  });

  const handleChange = (pagination, filters, sorter) => {
    setPaginationInfo(pagination);
  };

  const [visible, setVisible] = useState(false);
  const [clickedRow, setClickedRow] = useState({});

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const [logsList, setLogsList] = useState(null); // TO IMPROVE / What's supposed to store ?

  useEffect(() => {
    apiClient
      .get("/public_api/emission_logs")
      .then((response) => {
        // go to get in the database the emission request logs
        setLogsList(response.data);
      })
      .catch((err) => (
        <Alert message="Logs : An error occurred ..." type="error" />
      ));
  }, []);

  //Table columns
  //  requestIp, requestDate  and statusCode
  const columns = [
    {
      title: "ID",
      dataIndex: "_id",
      key: "ID",
    },
    {
      title: "IP",
      dataIndex: "requestIp",
      key: "REQUESTIP",
    },
    {
      title: "Date",
      dataIndex: "requestDate",
      key: "REQUESTDATE",
      sorter: true,
      defaultSortOrder: "descend",
      render: (requestDate) => {
        let newDate = moment(new Date(requestDate));
        return newDate.format("DD/MM/YYYY h:mm:ss a");
      },
    },
    {
      title: "Status code",
      dataIndex: "statusCode",
      key: "STATUSCODE",
      sorter: true,
    },
  ];

  const displayDrawerContent = (object) => {
    let drawerDisplay = [];

    Object.entries(object).map(([key, val]) => {
      if (val.isArray) {
        drawerDisplay.push(<p>{key} :</p>);
        for (let i = O; i < val.length; i++) {
          drawerDisplay.push(<p key={i}>- {val[i]}</p>);
        }
      } else if (typeof val === "object") {
        drawerDisplay.push(<p>{key} :</p>);
        Object.entries(val).map(([key2, val2]) => {
          drawerDisplay.push(
            <p key={key2}>
              - {key2} : {val2}
            </p>
          );
        });
      } else {
        drawerDisplay.push(
          <p>
            {key} : {val}
          </p>
        );
      }
    });

    return drawerDisplay;
  };

  // return <ul>{getAnimalsContent(animals)}</ul>;

  return (
    <div>
      <Helmet title="Logs" />
      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <div>
                <div className="table-responsive text-nowrap">
                  <Table
                    columns={columns}
                    dataSource={logsList || []}
                    pagination={{
                      ...paginationInfo,
                      total: logsList ? logsList.length : 0,
                    }}
                    onChange={handleChange}
                    onRow={(record, rowIndex) => {
                      return {
                        onClick: (event) => {
                          let index = 0;
                          let tmpId = logsList[index]._id;
                          while (
                            record._id !== tmpId &&
                            index < logsList.length
                          ) {
                            index++;
                            tmpId = logsList[index]._id;
                          }

                          setClickedRow(logsList[index]);
                          showDrawer();
                        },
                      };
                    }}
                    loading={!logsList}
                    rowKey={"_id"}
                    expandable={{
                      expandedRowRender: (record) => {
                        if (record.calculation === "Shipment")
                          return <ShipmentDetails calculation={record} />;
                        if (record.calculation === "Delivery")
                          return <DeliveryDetails calculation={record} />;
                        return "Not implemented";
                      },
                      rowExpandable: (record) =>
                        record.calculation === "Shipment" ||
                        record.calculation === "Delivery",
                    }}
                  />
                  <Drawer
                    title={"Request: " + clickedRow._id}
                    placement="right"
                    closable={false}
                    onClose={onClose}
                    visible={visible}
                    getContainer={false}
                    style={{ position: "fixed" }}
                    width="50%"
                  >
                    <Text>
                      <pre>{JSON.stringify(clickedRow, undefined, 2)}</pre>
                    </Text>
                  </Drawer>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Logs;
/*
{
                      Object.entries(clickedRow).map(([key, val]) => {
                        if (val.isArray){
                          <p>{key}: </p>
                          for (let i=O; i < data.length; i++){
                            <p>
                              - {val[index]}
                            </p>
                          }
                        }
                        else if (typeof(val) === "object"){
                          <p>{key}: </p>
                          Object.entries(val).map(([key2,val2]) => {
                            <p key={key2}>
                              {key2}: {val2}
                            </p>
                          })
                        }
                        else {
                          <p key={key}>
                          {key}: {val}
                          </p>
                        }          
                      })
                    }

if (val.isArray) {
      drawerDisplay = drawerDisplay + key + " :\n";
      for (let i = O; i < data.length; i++) {
        drawerDisplay = drawerDisplay + " - " + val[index] + "\n";
      }
    } else if (typeof val === "object") {
      drawerDisplay = drawerDisplay + key + " :\n";
      Object.entries(val).map(([key2, val2]) => {
        drawerDisplay = drawerDisplay + key2 + " : " + val2 + "\n";
      });
    } else {
      drawerDisplay = drawerDisplay + key + " : " + val + "\n";
    }

if (val.isArray) {
      drawerDisplay = drawerDisplay + "<p>" + key + " :</p>";
      for (let i = O; i < data.length; i++) {
        drawerDisplay = drawerDisplay + "<p>" + " - " + val[index] + "</p>";
      }
    } else if (typeof val === "object") {
      drawerDisplay = drawerDisplay + "<p>" + key + " :</p>";
      Object.entries(val).map(([key2, val2]) => {
        drawerDisplay = drawerDisplay + "<p>" + key2 + " : " + val2 + "</p>";
      });
    } else {
      drawerDisplay = drawerDisplay + "<p>" + key + " : " + val + "</p>";
    }

    title   .slice(0,12) + "..."

                        <Text code>{syntaxHighlight(clickedRow)}</Text>
                        <Text code>{JSON.stringify(clickedRow, undefined, 2)}</Text>

    const displayDrawerContent = object => {
    let drawerDisplay = [];

    Object.entries(object).map(([key, val]) => {
      if (val.isArray) {
        drawerDisplay.push(<p>{key} :</p>);
        for (let i = O; i < val.length; i++) {
          content.push(<p key={key}>- {val[index]}</p>);
        }
      } else if (typeof val === "object") {
        drawerDisplay.push(<p>{key} :</p>);
        Object.entries(val).map(([key2, val2]) => {
          drawerDisplay.push(<p key={key2}>- {key2} : {val2}</p>);
        });
      } else {
        drawerDisplay.push(<p>{key} : {val}</p>);
      };
    })

    return drawerDisplay;
  };

*/
