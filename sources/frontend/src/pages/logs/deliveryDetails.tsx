import { Descriptions } from "antd";
import NumberFormat from "react-number-format";

const DeliveryDetails: React.FC<{ calculation: any }> = function (props) {
  const calculation = props.calculation;
  return (
    <div>
      <Descriptions
        title={calculation.calculation}
        labelStyle={{ fontWeight: "bold", backgroundColor: "#cfb5f1" }}
        contentStyle={{ backgroundColor: "#f9f0ff" }}
        bordered
      >
        <Descriptions.Item label="Id" span={3}>
          {calculation.id}
        </Descriptions.Item>
        <Descriptions.Item label="Public Id" span={3}>
          {calculation.public_id}
        </Descriptions.Item>
        <Descriptions.Item label="Vehicle" span={3}>
          {calculation.vehicle}
        </Descriptions.Item>
        <Descriptions.Item label="Distance">
          <NumberFormat
            value={calculation.distance}
            displayType={"text"}
            thousandSeparator={" "}
            decimalScale={2}
          />{" "}
          km
        </Descriptions.Item>
        {calculation.from_address && (
          <Descriptions.Item label="From">
            <a
              href={
                "https://maps.google.com/?q=" +
                calculation.from_location?.coordinates[1] +
                "," +
                calculation.from_location?.coordinates[0]
              }
              target="_blank"
            >
              {calculation.from_address}
            </a>
          </Descriptions.Item>
        )}
        {calculation.to_address && (
          <Descriptions.Item label="To">
            <a
              href={
                "https://maps.google.com/?q=" +
                calculation.to_location?.coordinates[1] +
                "," +
                calculation.to_location?.coordinates[0]
              }
              target="_blank"
            >
              {calculation.to_address}
            </a>
          </Descriptions.Item>
        )}
      </Descriptions>
    </div>
  );
};

export default DeliveryDetails;
