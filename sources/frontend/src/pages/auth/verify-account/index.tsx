import { Helmet } from "react-helmet";
import VerifyAccount from "@vb/components/Auth/VerifyAccount";
import {useLocation} from "react-router-dom";

const SystemVerifyAccount = () => {
  const search = useLocation().search;
  const code = new URLSearchParams(search).get('verificationCode');
  const userId = new URLSearchParams(search).get('userId');

  return (
    <div>
      <Helmet title="VerifyAccount" />
      <VerifyAccount code={code} userId={userId} />
    </div>
  );
};

export default SystemVerifyAccount;
