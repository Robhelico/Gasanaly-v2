import { Helmet } from "react-helmet";
import Error600 from "@vb/components/Errors/600";

const System600 = () => {
  return (
    <div>
      <Helmet title="Page 600" />
      <Error600 />
    </div>
  );
};

export default System600;
