import { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { Alert, Table, Tag } from "antd";
import moment from "moment";
import ShipmentDetails from "./shipmentDetails";
import DeliveryDetails from "./deliveryDetails";
import NumberFormat from "react-number-format";
import apiClient from "services/axios";

const Emissions = () => {
  //Filter and Sort
  const [paginationInfo, setPaginationInfo] = useState({
    current: 1,
    pageSize: 50,
  });

  const handleChange = (pagination, filters, sorter) => {
    setPaginationInfo(pagination);
  };

  const [data, setData] = useState(null); // TO IMPROVE / What's supposed to store ?

  useEffect(() => {
    // apiClient
    //   .get("/public_api/emission")
    //   .then((response) => {
    setData(
      // sample data
      // actual data should come from response
      [
        {
          client: ["some name", "test"],
          calculation: "Car",
          carbon: "456",
          created_date: Date.now(),
        },
        {
          client: "some name",
          calculation: "Airplane",
          carbon: "666",
          created_date: Date.now(),
        },
        {
          client: "some other name",
          calculation: "Motorbike",
          carbon: "9",
          created_date: Date.now(),
        },
      ]
    );
    // })
    // .catch((err) => (
    //   <Alert message="Emissions : An error occurred ..." type="error" />
    // ));
  }, []);

  //Table columns
  const columns = [
    {
      title: "Client",
      dataIndex: "client", // dataIndex: ["client", "legal_name"], // the strings'array should theorically work
      key: "CLIENT",
    },
    {
      title: "Type",
      dataIndex: "calculation",
      key: "CALCULATION",
      filters: [
        {
          text: "Shipment",
          value: "Shipment",
        },
        {
          text: "Delivery",
          value: "Delivery",
        },
      ],
      sorter: true,
      render: (calculation) => {
        let color = "red";
        if (calculation === "Shipment") {
          color = "blue";
        }
        if (calculation === "Delivery") {
          color = "purple";
        }
        return (
          <Tag color={color} key={calculation}>
            {calculation.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Carbon (in kg)",
      dataIndex: "carbon",
      key: "CARBON",
      sorter: true,
      render: (carbon) => {
        return (
          <NumberFormat
            value={carbon}
            displayType={"text"}
            thousandSeparator={" "}
            decimalScale={5}
          />
        );
      },
    },
    {
      title: "Date",
      dataIndex: "created_date",
      key: "CREATED_DATE",
      sorter: true,
      defaultSortOrder: "descend",
      render: (created_date) => {
        let date = moment(new Date(created_date));
        return date.format("MM/DD/YYYY h:mm a");
      },
    },
  ];

  return (
    <div>
      <Helmet title="Emissions" />
      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <div>
                <div className="table-responsive text-nowrap">
                  <Table
                    columns={columns}
                    dataSource={data || []}
                    pagination={{
                      ...paginationInfo,
                      total: data ? data.length : 0,
                    }}
                    onChange={handleChange}
                    loading={!data}
                    rowKey={"id"}
                    expandable={{
                      expandedRowRender: (record) => {
                        if (record.calculation === "Shipment")
                          return <ShipmentDetails calculation={record} />;
                        if (record.calculation === "Delivery")
                          return <DeliveryDetails calculation={record} />;
                        return "Not implemented";
                      },
                      rowExpandable: (record) =>
                        record.calculation === "Shipment" ||
                        record.calculation === "Delivery",
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Emissions;
