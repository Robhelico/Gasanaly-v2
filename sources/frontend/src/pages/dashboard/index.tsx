import { Helmet } from "react-helmet";
import { Alert, Spin } from "antd";
import NumberFormat from "react-number-format";
import { Line } from "react-chartjs-2";
import { useEffect, useState } from "react";
import moment from "moment";
import apiClient from "services/axios";

const Dashboard = () => {
  //Get Dashboard data
  const [data, setData] = useState<any>(null); // find lthe dashboard data type later

  useEffect(() => {
    apiClient
      .get("/test")
      .then((response) => {
        console.log("apiCall");
        setData({
          // sample data
          // actual data should come from response
          dashboard: {
            total_per_day: [
              { date: Date.now(), total_carbon: 100, total_calculations: 200 },
              { date: Date.now(), total_carbon: 50, total_calculations: 100 },
              { date: Date.now(), total_carbon: 10, total_calculations: 20 },
            ],
          },
        });
      })
      .catch((err) => (
        <Alert message="Dashboard : An error occurred ..." type="error" />
      ));
  }, []);

  //Display the line graph
  const [lineData, setLineData] = useState({});
  useEffect(() => {
    if (data && data.dashboard.total_per_day) {
      console.log(data);
      let labels: string[] = [];
      let dataSetCarbon: number[] = [];
      let dataSetCalculation: number[] = [];

      data.dashboard.total_per_day.forEach((item: any) => {
        // TO IMPROVE
        let date = moment(new Date(item.date));
        labels.push(date.format("MM/DD/YYYY"));

        dataSetCarbon.push(item.total_carbon);
        dataSetCalculation.push(item.total_calculations);
      });

      setLineData({
        labels: labels,
        datasets: [
          {
            label: "Total Carbon",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: dataSetCarbon,
          },
          {
            label: "Emissions",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#236799",
            borderColor: "#236799",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#236799",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "#236799",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: dataSetCalculation,
          },
        ],
      });
    }
  }, [data]);

  //Display the evolution numbers
  const displayEvolution = (evolution: any) => {
    // TO IMPROVE
    if (!evolution) {
      return "";
    }
    if (evolution > 0) {
      return (
        <span className="text-success font-size-12 ml-3">
          &uarr; {evolution} %
        </span>
      );
    }

    return (
      <span className="text-danger font-size-12 ml-3">
        &darr; {evolution} %
      </span>
    );
  };

  return (
    <div>
      <Spin spinning={!data}>
        <Helmet title="Dashboard" />
        <div className="row">
          <div className="col-lg-6 col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="card-body overflow-hidden position-relative">
                  <div className="font-size-36 font-weight-bold text-dark line-height-1 mt-2">
                    <NumberFormat
                      value={data?.dashboard.total_carbon}
                      displayType={"text"}
                      thousandSeparator={" "}
                    />
                    {displayEvolution(data?.dashboard?.total_carbon_evolution)}
                  </div>
                  <div className="text-uppercase mb-1">
                    Total Carbon (in kg)
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="card-body overflow-hidden position-relative">
                  <div className="font-size-36 font-weight-bold text-dark line-height-1 mt-2">
                    <NumberFormat
                      value={data?.dashboard.total_calculations}
                      displayType={"text"}
                      thousandSeparator={" "}
                    />
                    {displayEvolution(
                      data?.dashboard?.total_calculations_evolution
                    )}
                  </div>
                  <div className="text-uppercase mb-1">Emissions</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-body">
                <Line data={lineData} />
              </div>
            </div>
          </div>
        </div>
      </Spin>
    </div>
  );
};

export default Dashboard;
