import axios from "axios";
import store from "store";
import { notification } from "antd";

const apiClient = axios.create({
  baseURL: process.env.REACT_APP_SERVER_ENDPOINT,
  // timeout: 1000,
  // headers: { 'X-Custom-Header': 'foobar' }
});

apiClient.interceptors.request.use((request) => {
  // add available accessToken to the requests
  const accessToken = store.get("accessToken");
  if (accessToken) {
    request.headers.Authorization = `Bearer ${accessToken}`;
    request.headers.AccessToken = accessToken;
  }
  return request;
});

apiClient.interceptors.response.use(undefined, (error) => {
  // manage errrors warning and display them
  // Errors handling
  const { response } = error;
  if (response?.data) {
    notification.warning({
      message: response.data,
    });
  } else {
    notification.warning({
      message: "Something unexpected happened",
    });
    console.error(error);
  }
});

export default apiClient;
