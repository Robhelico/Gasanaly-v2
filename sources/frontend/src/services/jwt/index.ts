import apiClient from "services/axios";
import store from "store";

export async function login(email: string, password: string) {
  console.log("client login request");
  return apiClient
    .post(
      "/auth/login",
      {
        email,
        password,
      }
      // {"Content-Type": "application/json"}
    )
    .then((response) => {
      if (response) {
        const { accessToken } = response.data;
        if (accessToken) {
          store.set("accessToken", accessToken);
        }
        return response.data;
      }
      return false;
    })
    .catch((err) => console.log(err));
}

export async function register(email: string, password: string, name: string) {
  return apiClient
    .post(
      "/auth/register",
      {
        email,
        password,
        name,
      },
      { headers: { "Content-Type": "application/json" } }
    )
    .then((response) => {
      if (response) {
        const { accessToken } = response.data;
        if (accessToken) {
          store.set("accessToken", accessToken);
        }
        return response.data;
      }
      return false;
    })
    .catch((err) => console.log(err));
}

export async function verifyAccount(userId: any, verificationCode: string) {
  return apiClient
    .post(
      "/auth/verify-account",
      {
        userId,
        verificationCode,
      },
      { headers: { "Content-Type": "application/json" } }
    )
    .then((response) => {
      if (response) {
        const { accessToken } = response.data;
        if (accessToken) {
          store.set("accessToken", accessToken);
        }
        return response.data;
      }
      return false;
    })
    .catch((err) => console.log(err));
}

export async function currentAccount() {
  console.log("client currentAccount function");
  return apiClient
    .get("/auth/account")
    .then((response) => {
      if (response) {
        const { accessToken } = response.data;
        if (accessToken) {
          store.set("accessToken", accessToken);
        }
        return response.data;
      }
      return false;
    })
    .catch((err) => console.log(err));
}

export async function logout() {
  return apiClient
    .get("/auth/logout")
    .then(() => {
      store.remove("accessToken");
      return true;
    })
    .catch((err) => console.log(err));
}
