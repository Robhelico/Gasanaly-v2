export default async function getMenuData() {
  return [
    // VB:REPLACE-START:MENU-CONFIG
    {
      title: "Dashboard",
      key: "__dashboard",
      url: "/dashboard",
      icon: "fe fe-home",
    },
    {
      title: "Emissions",
      key: "__emissions",
      url: "/emissions",
      icon: "fe fe-cloud",
    },
    // VB:REPLACE-END:MENU-CONFIG
    {
      title: "Logs",
      key: "__logs",
      url: "/logs",
      icon: "fe fe-database",
    },
  ];
}
