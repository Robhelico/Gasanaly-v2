const actions = {
  SET_STATE: "user/SET_STATE",
  LOGIN: "user/LOGIN",
  REGISTER: "user/REGISTER",
  VERIFY_ACCOUNT: "user/VERIFY_ACCOUNT",
  LOAD_CURRENT_ACCOUNT: "user/LOAD_CURRENT_ACCOUNT",
  LOGOUT: "user/LOGOUT",
};

export default actions;
