import { all, takeEvery, put, call, select } from "redux-saga/effects";
import { notification } from "antd";
import { history } from "index";
import * as jwt from "services/jwt";
import actions from "./actions";

const mapAuthProviders = {
  jwt: {
    login: jwt.login,
    register: jwt.register,
    verifyAccount: jwt.verifyAccount,
    currentAccount: jwt.currentAccount,
    logout: jwt.logout,
  },
};

export function* LOGIN({ payload }) {
  // TO IMPROVE
  const { email, password } = payload;
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: true,
    },
  });
  const { authProvider: autProviderName } = yield select(
    (state) => state.settings
  );
  const success = yield call(
    mapAuthProviders[autProviderName].login,
    email,
    password
  );
  if (success) {
    yield put({
      type: "user/LOAD_CURRENT_ACCOUNT",
    });
    yield history.push("/");
    notification.success({
      message: "Logged In",
    });
  }
  if (!success) {
    yield put({
      type: "user/SET_STATE",
      payload: {
        loading: false,
      },
    });
  }
}

export function* REGISTER({ payload }) {
  const { email, password, name } = payload;
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: true,
    },
  });
  const { authProvider } = yield select((state) => state.settings);
  const success = yield call(
    mapAuthProviders[authProvider].register,
    email,
    password,
    name
  );
  if (success) {
    yield put({
      type: "user/LOAD_CURRENT_ACCOUNT",
    });
    yield history.push("/");
    notification.success({
      message: "User registered. Waiting for email validation ...",
    });
  }
  if (!success) {
    yield put({
      type: "user/SET_STATE",
      payload: {
        loading: false,
      },
    });
  }
}

export function* LOAD_CURRENT_ACCOUNT() {
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: true,
    },
  });
  const { authProvider } = yield select((state) => state.settings);
  const response = yield call(mapAuthProviders[authProvider].currentAccount);
  if (response) {
    const { id, email, name, avatar, role } = response;
    yield put({
      type: "user/SET_STATE",
      payload: {
        id,
        name,
        email,
        avatar,
        role,
        authorized: true,
      },
    });
  }
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: false,
    },
  });
}

export function* LOGOUT() {
  const { authProvider } = yield select((state) => state.settings);
  yield call(mapAuthProviders[authProvider].logout);
  yield put({
    type: "user/SET_STATE",
    payload: {
      id: "",
      name: "",
      role: "",
      email: "",
      avatar: "",
      authorized: false,
      loading: false,
    },
  });
}

export function* VERIFY_ACCOUNT({ payload }) {
  const { code, userId } = payload;
  yield put({
    type: "user/SET_STATE",
    payload: {
      loading: true,
    },
  });
  const { authProvider } = yield select((state) => state.settings);
  const success = yield call(
    mapAuthProviders[authProvider].verifyAccount,
    userId,
    code
  );

  if (success) {
    yield put({
      //check it late
      type: "user/LOAD_CURRENT_ACCOUNT",
    });
    yield history.push("/");
    notification.success({
      message: "User registered succesfully.",
    });
  }
  if (!success) {
    yield history.push("/auth/600");
    notification.success({
      message: "User registered not succesfully.",
    });
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.LOGIN, LOGIN),
    takeEvery(actions.REGISTER, REGISTER),
    takeEvery(actions.VERIFY_ACCOUNT, VERIFY_ACCOUNT),
    takeEvery(actions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    takeEvery(actions.LOGOUT, LOGOUT),
    // LOAD_CURRENT_ACCOUNT(), // run once on app load to check user auth
  ]);
}
