import { lazy, Suspense } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import { CSSTransition, SwitchTransition } from "react-transition-group";
import { connect } from "react-redux";

import Layout from "layouts";

const routes = [
  // VB:REPLACE-START:ROUTER-CONFIG
  {
    path: "/dashboard",
    Component: lazy(() => import("pages/dashboard")),
    exact: true,
  },
  {
    path: "/emissions",
    Component: lazy(() => import("pages/emissions")),
    exact: true,
  },
  // VB:REPLACE-END:ROUTER-CONFIG
  {
    path: "/logs",
    Component: lazy(() => import("pages/logs")),
    exact: true,
  },
  {
    path: "/auth/login",
    Component: lazy(() => import("pages/auth/login")),
    exact: true,
  },
  {
    path: "/auth/forgot-password",
    Component: lazy(() => import("pages/auth/forgot-password")),
    exact: true,
  },
  {
    path: "/auth/register",
    Component: lazy(() => import("pages/auth/register")),
    exact: true,
  },

  {
    path: "/auth/verify-account",
    Component: lazy(() => import("pages/auth/verify-account")),
    exact: true,
  },
  {
    path: "/auth/lockscreen",
    Component: lazy(() => import("pages/auth/lockscreen")),
    exact: true,
  },
  {
    path: "/auth/404",
    Component: lazy(() => import("pages/auth/404")),
    exact: true,
  },
  {
    path: "/auth/500",
    Component: lazy(() => import("pages/auth/500")),
    exact: true,
  },
  {
    path: "/auth/600",
    Component: lazy(() => import("pages/auth/600")),
    exact: true,
  },
];

const mapStateToProps = ({ settings }: any) => ({
  // TO IMPROVE
  routerAnimation: settings.routerAnimation,
});

const Router = ({ history, routerAnimation }: any) => {
  // TO IMPROVE
  return (
    <ConnectedRouter history={history}>
      <Layout>
        <Route
          render={(state) => {
            const { location } = state;
            return (
              <SwitchTransition>
                <CSSTransition
                  key={location.pathname}
                  appear
                  classNames={routerAnimation}
                  timeout={routerAnimation === "none" ? 0 : 300}
                >
                  <Switch location={location}>
                    {/* VB:REPLACE-NEXT-LINE:ROUTER-REDIRECT */}
                    <Route
                      exact
                      path="/"
                      render={() => <Redirect to="/dashboard" />}
                    />
                    {routes.map(({ path, Component, exact }) => (
                      <Route
                        path={path}
                        key={path}
                        exact={exact}
                        render={() => {
                          return (
                            <div className={routerAnimation}>
                              <Suspense fallback={null}>
                                <Component />
                              </Suspense>
                            </div>
                          );
                        }}
                      />
                    ))}
                    <Redirect to="/auth/404" />
                  </Switch>
                </CSSTransition>
              </SwitchTransition>
            );
          }}
        />
      </Layout>
    </ConnectedRouter>
  );
};

export default connect(mapStateToProps)(Router);
