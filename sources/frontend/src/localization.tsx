import { ConfigProvider } from "antd";
import { IntlProvider } from "react-intl";
import { connect } from "react-redux";

import english from "./locales/en-GB";
import french from "./locales/fr-FR";
import russian from "./locales/ru-RU";
import chinese from "./locales/zh-CN";

const locales = {
  "en-GB": english,
  "fr-FR": french,
  "ru-RU": russian,
  "zh-CN": chinese,
};

const mapStateToProps = ({ settings }: any) => ({ settings }); // TO IMPROVE

const Localization = ({ children, settings: { locale } }: any) => {
  // TO IMPROVE
  const currentLocale = locales[locale];
  return (
    <ConfigProvider locale={currentLocale.localeAntd}>
      <IntlProvider
        locale={currentLocale.locale}
        messages={currentLocale.messages}
      >
        {children}
      </IntlProvider>
    </ConfigProvider>
  );
};

export default connect(mapStateToProps)(Localization);
