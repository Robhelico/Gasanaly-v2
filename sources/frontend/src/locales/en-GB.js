import localeAntd from "antd/es/locale/en_US";

const messages = {
  "topBar.issuesHistory": "Issues",
  "topBar.projectManagement": "Projects",
  "topBar.typeToSearch": "Search...",
  "topBar.findPages": "Find pages...",
  "topBar.actions": "Actions",
  "topBar.status": "Status",
  "topBar.profileMenu.hello": "Hello",
  "topBar.profileMenu.billingPlan": "Billing Plan",
  "topBar.profileMenu.role": "Role",
  "topBar.profileMenu.email": "Email",
  "topBar.profileMenu.phone": "Phone",
  "topBar.profileMenu.editProfile": "Profile",
  "topBar.profileMenu.logout": "Sign Out",
};

export default {
  locale: "en-GB",
  localeAntd,
  messages,
};
