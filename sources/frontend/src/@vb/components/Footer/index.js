import React from "react";
import { connect } from "react-redux";
import style from "./style.module.scss";

const mapStateToProps = ({ settings }) => ({ settings });

const Footer = ({ settings: { logo } }) => {
  return (
    <div className={style.footer}>
      <div className={style.footerInner}>
        <a
          href="https://www.google.com"
          target="_blank"
          rel="noopener noreferrer"
          className={style.logo}
        >
          <strong className="mr-2">{logo}</strong>
        </a>
        <br />
        <p className="mb-0">
          Copyright © {new Date().getFullYear()}{" "}
          <a
            href="https://www.google.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Test APP
          </a>
          {" | "}
          <a
            href="https://www.google.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Privacy Policy
          </a>
        </p>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(Footer);
