import React from "react";
import { Link } from "react-router-dom";

const Error600 = () => {
  return (
    <div className="container pl-5 pr-5 pt-5 pb-5 mb-auto text-dark font-size-32">
      <div className="font-weight-bold mb-3">Database Error</div>
      <div className="text-gray-6 font-size-24">
        The validation link is not valid anymore. A new one has been sent to your address.
      </div>
      <div className="font-weight-bold font-size-70 mb-1">600 —</div>
      <Link to="/auth/login" className="btn btn-outline-primary width-100">
        Go Back
      </Link>
    </div>
  );
};

export default Error600;
