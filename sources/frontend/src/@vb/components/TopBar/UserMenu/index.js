import React from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import { Menu, Dropdown, Avatar } from "antd";
import styles from "./style.module.scss";

const mapStateToProps = ({ user }) => ({ user });

const ProfileMenu = ({ dispatch, user }) => {
  const logout = (e) => {
    e.preventDefault();
    dispatch({
      type: "user/LOGOUT",
    });
  };

  const menu = (
    <Menu selectable={false}>
      <Menu.Item selectable={false}>
        <strong>{user.name || "Anonymous"}</strong>
        <div>{user.role || "—"}</div>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item>
        <a href="#" onClick={(e) => e.preventDefault()}>
          <FormattedMessage id="topBar.profileMenu.editProfile" />
        </a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item>
        <a href="#" onClick={logout}>
          <FormattedMessage id="topBar.profileMenu.logout" />
        </a>
      </Menu.Item>
    </Menu>
  );
  return (
    <Dropdown overlay={menu} trigger={["click"]}>
      <div className={styles.dropdown}>
        <Avatar
          className={styles.avatar}
          shape="square"
          size="large"
          src={user.avatar || ""}
          icon={<UserOutlined />}
        />
      </div>
    </Dropdown>
  );
};

export default connect(mapStateToProps)(ProfileMenu);
