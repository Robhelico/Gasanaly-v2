import React from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
// import Sidebar from '@vb/components/Sidebar'
// import SupportChat from '@vb/components/SupportChat'
import style from "./style.module.scss";

const mapStateToProps = ({ settings }) => ({
  isGrayTopbar: settings.isGrayTopbar,
  isCardShadow: settings.isCardShadow,
  isSquaredBorders: settings.isSquaredBorders,
  isBorderless: settings.isBorderless,
  authPagesColor: settings.authPagesColor,
});

const AuthLayout = ({
  children,
  isCardShadow,
  isSquaredBorders,
  isBorderless,
  authPagesColor,
}) => {
  return (
    <Layout className="vb__layout">
      <Layout.Content>
        <div
          className={classNames(`${style.container}`, {
            vb__layout__squaredBorders: isSquaredBorders,
            vb__layout__cardsShadow: isCardShadow,
            vb__layout__borderless: isBorderless,
            [style.white]: authPagesColor === "white",
            [style.gray]: authPagesColor === "gray",
          })}
          style={{
            backgroundImage:
              authPagesColor === "image"
                ? "url(resources/images/content/photos/8.jpeg)"
                : "",
          }}
        >
          <div className="mb-5">{children}</div>
          <div className="mt-auto pb-5 pt-5">
            <ul
              className={`${style.footerNav} list-unstyled d-flex mb-0 flex-wrap justify-content-center`}
            >
              <li>
                <a href="#" onClick={(e) => e.preventDefault()}>
                  Terms of Use
                </a>
              </li>
              <li>
                <a href="#" onClick={(e) => e.preventDefault()}>
                  Compliance
                </a>
              </li>
              <li>
                <a href="#" onClick={(e) => e.preventDefault()}>
                  Support
                </a>
              </li>
              <li>
                <a href="#" onClick={(e) => e.preventDefault()}>
                  Contacts
                </a>
              </li>
              <li>
                <a href="#" onClick={(e) => e.preventDefault()}>
                  About
                </a>
              </li>
              <li>
                <a href="#" onClick={(e) => e.preventDefault()}>
                  Advertising
                </a>
              </li>
            </ul>
            <div className="text-center">
              Copyright © {new Date().getFullYear()}{" "}
              <a
                href="https://www.google.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                Test APP
              </a>
              {" | "}
              <a
                href="https://www.google.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                Privacy Policy
              </a>
            </div>
          </div>
        </div>
      </Layout.Content>
    </Layout>
  );
};

export default withRouter(connect(mapStateToProps)(AuthLayout));
