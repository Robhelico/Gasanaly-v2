import morgan from 'morgan';


morgan.token('param', function(req, res, param) {
    return req.params[param];
});

morgan.token('query', function(req, res, property) {
    return req.query[property];
});

// app.use(morgan(':method :host :status :param[id] :res[content-length] - :response-time ms')); // maybe combine with function below 

const emissionRequestLogs = morgan(
        ":remote-addr [:date[iso]] fuel-type_:query[fuel_type] consumption_:query[fuel_consumption_per_100km] distance_:query[distance_traveled_in_km] :res[content-type] content-lenght_:res[content-length] status_:status :response-time ms"
    ,{} //options
);

export {
    emissionRequestLogs
}