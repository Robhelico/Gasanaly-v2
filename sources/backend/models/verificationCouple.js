import mongoose from 'mongoose';

const verificationCoupleSchema = new mongoose.Schema({
  userId: { type: String, required: true, unique: true },
  verificationCode: { type: String, required: true}
},
{ timestamps: true });

const VerificationCoupleModel = mongoose.model("verificationCouple", verificationCoupleSchema);

export { VerificationCoupleModel };