import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  avatar: { type: String },
  role: { type: String, required: true },
  isVerified: { type: Boolean, required: true }
},
{ timestamps: true });

const UserModel = mongoose.model("user", userSchema);

export { UserModel };