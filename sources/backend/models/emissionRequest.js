import mongoose from 'mongoose';

const emissionRequestSchema = new mongoose.Schema({ // need to modify to make it fit with morgan
//   email: { type: String, required: true, unique: true },
  requestIp: { type: String, required: true },
  requestDate: { type: Date, required: true },
  responseTimeInMs: { type: Number, required: true },
  statusCode: { type: Number, required: true },
  responseHeaders: { type: Array, required: true },
  fuelType: { type: String, required: true },
  fuelConsumptionPer100km: { type: Number },
  distanceTraveledInKm: { type: Number, required: true },
  appraisal: {type: Object, required: true}, // could be better
},
{ timestamps: true });

const EmissionRequestModel = mongoose.model("emissionRequest", emissionRequestSchema);

export { EmissionRequestModel };