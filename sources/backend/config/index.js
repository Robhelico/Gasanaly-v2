const {
  PORT,
  TOKEN_KEY,
  MONGODB_URI,
  SENDGRID_API_KEY
} = process.env;

const port = PORT;
const tokenKey = TOKEN_KEY;
const mongodbUri = MONGODB_URI;
const sendgridApiKey = SENDGRID_API_KEY;


export {
  port,
  tokenKey,
  mongodbUri,
  sendgridApiKey
};