import { VerificationCoupleModel } from '../models/verificationCouple.js';


//Add VerificationCouple
const addVerificationCouple = async (object) => { // need data checking somewhere
    try {
        console.log("addVerificationCouple function");       
        console.log("object ID: " + object.userId + "\nobject random string : " + object.verificationCode);
        
        const verificationCouple = new VerificationCoupleModel(object);
        console.log("Mongoose verificationCouple created");
        return verificationCouple.save()
            .then((result) => {
                console.log("VerificationCouple added !\n" + result);
                return result;
            })
            .catch((error) => {
                console.log(error.message);
                return error;
            });                         
    } catch (error) {
        return error;
    }
};

//Get one VerificationCouple
const getVerificationCouple = async (object) => {
    console.log("getVerificationCouple function");
    try {
        console.log(" object : " + object);
        console.log("userId : " + object.userId);
        if (object._id) { // if an ID is given
            console.log("by _id");
            return VerificationCoupleModel.findById(object._id)
                .then((result) => {
                    console.log("couple found and returned");
                    return result;
                }).catch((error) => {
                    console.log("couple not found");
                    return error;
                })
        } 
        else if (object.userId) { // if an userId is given
            console.log("by userId");
            return VerificationCoupleModel.findOne({ userId: object.userId })
                .then((result) => {
                    return result;
                }).catch((error) => {
                    return error;
                })
        }    
    } catch (error) {
        return error;
    }
};

//Get All VerificationCouples
const getAllVerificationCouples = async () => {
    console.log("getAllVerificationCouples function");
    try {
       return VerificationCoupleModel.find()
        .then((result) => {
            return result;
        })
        .catch((error) => {
            return error;
        })
    } catch (error) {
        return error;
    }
};

export {
    addVerificationCouple,
    getVerificationCouple,
    getAllVerificationCouples
}