import { UserModel } from '../models/user.js';

//Add User
const addUser = async (object) => { // need data checking somewhere
    try {
        console.log("addUser function");
        
        const user = new UserModel(object);
        console.log("Mongoose user created");
        return user.save()
            .then((result) => {
                console.log("User added !\n" + result);
                return result;
            })
            .catch((error) => {
                console.log("error");
                return error;
            });                         
    } catch (error) {
        return error;
    }
};

//Get one User
const getUser = async (object) => {
    console.log("getUser function");
    try {
        if (object._id) { // if an ID is given
            return UserModel.findById(object._id)
                .then((result) => {
                    return result;
                }).catch((error) => {
                    return error;
                })
        } 
        else if (object.email) { // if an email is given
            return UserModel.findOne({ email: object.email })
                .then((result) => {
                    console.log("win 1");
                    return result;
                }).catch((error) => {
                    return error;
                })
        }    
    } catch (error) {
        console.log("2");
        return error;
    }
};

//Get All Users
const getAllUsers = async () => {
    console.log("getAllUsers function");
    try {
       return UserModel.find()
        .then((result) => {
            return result;
        })
        .catch((error) => {
            return error;
        })
    } catch (error) {
        return error;
    }
};

export {
    addUser,
    getUser,
    getAllUsers
}