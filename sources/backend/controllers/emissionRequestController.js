import { EmissionRequestModel } from '../models/emissionRequest.js';

//Add EmissionRequest
const addEmissionRequest = async (object) => { // need data checking somewhere
    try {
        console.log("addEmissionRequest function");
        
        const emissionRequest = new EmissionRequestModel(object);
        console.log("Mongoose emission request created");
        return emissionRequest.save()
            .then((result) => {
                console.log("EmissionRequest added !\n" + result);
                return result;
            })
            .catch((error) => {
                return error;
            });                         
    } catch (error) {
        return error;
    }
};

//Get one EmissionRequest
const getEmissionRequest = async (object) => {
    console.log("getEmissionRequest function");
    try {
        if (object._id) { // if an ID is given
            return EmissionRequestModel.findById(object._id)
                .then((result) => {
                    return result;
                }).catch((error) => {
                    return error;
                })
        } 
        else if (object.id) { // if an IP is given
            return EmissionRequestModel.findOne({ ip: object.ip })
                .then((result) => {
                    return result;
                }).catch((error) => {
                    return error;
                })
        }    
    } catch (error) {
        return error;
    }
};

//Get all EmissionRequestLogs
const getAllEmissionRequests = async () => {
    console.log("getAllEmissionRequests function");
    try {
        return EmissionRequestModel.find()
            .then((result) => {
                console.log("control");
                console.log(result);
                return result;
            }).catch((error) => {
                return error;
        }) 
    } catch (error) {
        return error;
    }
};

export {
    addEmissionRequest,
    getEmissionRequest,
    getAllEmissionRequests
}