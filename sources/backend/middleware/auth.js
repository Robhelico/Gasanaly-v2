import jwt from 'jsonwebtoken';
import { tokenKey } from '../config/index.js';

const verifyToken = (req, res, next) => {
  console.log("verifying the token");

  const token =
    req.body.accessToken || req.query.accessToken || req.header("x-access-token") || req.header("accessToken");

  
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  };
  try {
    const decoded = jwt.verify(token, tokenKey);

    req.body.user = decoded;
  } 
  catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

export default verifyToken;