import express from 'express';
// const cors = require('cors');
import { tokenKey, sendgridApiKey }  from '../config/index.js';

import auth from '../middleware/auth.js';
import bcrypt from 'bcryptjs';
import randomstring from 'randomstring';


import { addUser, getUser, getAllUsers } from '../controllers/userController.js';

import sgMail from '@sendgrid/mail';
// import { AnyObject } from 'mongoose';

const miniApp = express.Router();

const [days, hours, minutes] = [0,2,0];
const jwtConfig = {
  tokenKey,  // fixed tokenKey signature
  expiresIn: days * 24 * 60 * 60 * 1000 + hours * 60 * 60 * 1000 + minutes * 60 * 1000, // days * hours * minutes * seconds * milliseconds
};

console.log("test route");

miniApp.use(express.json());
// miniApp.use(cors(/*{origin:"http://client.carbonapp.localhost/"}*/));
miniApp.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, accessToken, authorization");
  next();
});


// *Dev User management* TO REMOVE BEFORE TRUE DEPLOYMENT
//addUser
miniApp.post("/test/addUser", /*auth,*/ async (req, res) => {
  console.log("/test/addUser endpoint");
  req.body.id ? console.log("data received") : console.log("no data received");
  
  const addedUser = await addUser(req.body);

  if (addedUser.email){
    res.status(200).send(addedUser);
  }
  else{
    res.status(400).send(addedUser);
  }
});

//getUser
miniApp.post("/test/getUser", /*auth,*/ async (req, res) => {
  console.log("/test/getUser endpoint");
  const result = await getUser(req.body);
  if (result._id){ // user found
    res.status(200).send(result);
  }
  else { //error
    res.status(400).send(result);
  };
});

//getAllUsers
miniApp.post("/test/getAllUsers", /*auth,*/ async (req, res) => {
  console.log("/test/getAllUsers endpoint");
  const allUsers = await getAllUsers();
  if (Array.isArray(allUsers)){
    res.status(200).send(allUsers);
  } 
  else {
    res.status(400).send(allUsers);
  };
});

miniApp.post("/test/sendmail", async (req, res) => {
  try {
    const email = req.body.email;
    console.log("sendgrid key : " + sendgridApiKey);
    console.log("email : " + email);

    sgMail.setApiKey(sendgridApiKey);
    
    const msg = {
      to: email, // Change to your recipient
      from: 'rmonje@helius-systems.com', // Change to your verified sender
      subject: 'Gasanaly verification email',
      text: "If you are registering your new Gasanaly account, let's end it now. Click the following link : http://server.carbonapp.localhost/auth/verify-account?key=\n\n",
      html: "<p>If you are registering your new Gasanaly account, let's end it now. Click the link down below :</p><br><a href='http://server.carbonapp.localhost/auth/verify-account?key=' rel='noreferrer noopener'>\n\n", //put the right link/endpoint
    }
    console.log("msg : " + msg.text);

    sgMail
      .send(msg)
      .then(() => {
        console.log('Email sent');
        res.status(200).send("succes, email sent")
      })
      .catch((error) => {
        console.error(error);
        res.status(500).send(error);
      });
  }
  catch (error) {
    res.status(500).send(error);
  }
});

// *Test*
miniApp.get("/test", auth, (req,res) => {
  console.log("/test endpoint");
  res.status(200).send("No test set up for now");
});


// Home
miniApp.get("/home", (req, res) => {
  res.status(200).send("You are home now ☕");
});

miniApp.get("/", (req, res) => {
  res.status(200).send("You are home now ☕");
});

// Welcome
miniApp.get("/welcome", auth, (req, res) => {
  res.status(200).send("Welcome 🙌 ");
});

miniApp.post("/welcome", auth, (req, res) => {
  const {name} = req.body;
  if(name)
    res.status(200).send("Welcome " + req.body.name + " 🙌 ");
  else {
    res.status(200).send("Welcome 🙌 ");
  }
});

miniApp.get("/test/bcrypt", async (req,res) => {
  console.log("/test/bcrypt endpoint");
  let stringo = "";
  for(let i = 0; i<9; i++){

    const verificationCode = randomstring.generate({
      length: 40,
      charset: 'alphanumeric'
    }); 
    stringo += "random string " + i + " : " + verificationCode + "\n";

    const cookedString = await bcrypt.hash(verificationCode, 10);
    stringo += "cooked : " + cookedString + "\n\n";
  }
  res.status(200).send(stringo);
});

export default miniApp;