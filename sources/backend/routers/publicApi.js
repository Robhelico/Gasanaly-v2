import express from 'express';
import { addEmissionRequest, getAllEmissionRequests } from '../controllers/emissionRequestController.js';
import auth from '../middleware/auth.js';

const miniApp = express.Router();

console.log("public_api route");

miniApp.get("", (req,res) => {
    console.log("public_api endpoint");
    res.status(200).send("You are where you wanted to be.");
});

miniApp.get("/emission", /*auth,*/ (req, res) => {
    // source of densities : https://ecoscore.be/en/info/ecoscore/co2

    //const _startAt = process.hrtime();
    const _beginning = new Date();

    console.log("/public_api/emission endpoint");

    let statusCode = 0;
    const requestIp = req.ip;
    const requestDate = _beginning.toISOString();
    const fuelType = req.query.fuel_type;
    const fuelConsumptionPer100km = Number(req.query.fuel_consumption_per_100km); // L of CO2/100km
    const distanceTraveledInKm = Number(req.query.distance_traveled_in_km); // km
    let concentration = 0;
    let amount = -1;
    let appraisal = {};
    const responseHeaders = res.getHeaderNames();

    try {

        if(typeof(fuelType)!==undefined && typeof(fuelConsumptionPer100km)!==undefined && typeof(distanceTraveledInKm)!==undefined){

            console.log("lets calculate");

            switch(fuelType) {
                case "diesel" :
                    concentration = 2640; // g of CO2/L
                break;
                case "petrol" :
                    concentration = 2392; // g of CO2/L
                break;
                case "lpg" :
                    concentration = 1665; // g of CO2/L
                break;
                case "cng-low_calorific" :
                    concentration = 2252; // g of CO2/L
                break;
                case "cng-high_calorific" :
                    concentration = 2666; // g of CO2/L
                break;
                default :
                    concentration = undefined;
                    res.status(400).send("This fuel type doesn't exist.");
            };

            if (concentration) {
                amount = concentration * distanceTraveledInKm * fuelConsumptionPer100km / 100;
                const unit = "g";

                appraisal = {
                    co2 : {
                        amount,
                        unit
                    }
                };
                statusCode = 200;
                res.status(statusCode).send(appraisal);    
            }            
        }
        else {
            statusCode = 400;
            res.status(statusCode).send("At least one parameter is not defined.");
        };

    }
    catch (error) {
        statusCode = 400;
        res.status(statusCode).send(error);
    };

    // const responseTime = process.hrtime(_startAt);
    const _ending = new Date();
    const responseTimeInMs = _ending - _beginning // time in milliseconds

    const logObject = {
        requestIp,
        requestDate,
        responseTimeInMs,
        statusCode,
        responseHeaders,
        fuelType,
        fuelConsumptionPer100km,
        distanceTraveledInKm,
        appraisal
    };
    console.log(logObject);

    addEmissionRequest(logObject);
});

miniApp.get("/emission_logs", async (req,res) => {
    console.log("public_api/emission_logs endpoint");
    try {
        console.log("1");
        const logs = await getAllEmissionRequests();
        console.log("2");
        console.log(logs);
        res.status(200).send(logs);
    }
    catch (error) {
        console.log("3");
        res.status(400).send(error);
    }
});    

export default miniApp;