import express from 'express';
import { tokenKey, sendgridApiKey }  from '../config/index.js';

import jwt from 'jsonwebtoken';
import auth from '../middleware/auth.js';
import bcrypt from 'bcryptjs';
import randomstring from 'randomstring';


import { addUser, getUser } from '../controllers/userController.js';
import { UserModel } from '../models/user.js';
import { addVerificationCouple, getVerificationCouple } from '../controllers/verificationCoupleController.js';
import { VerificationCoupleModel } from '../models/verificationCouple.js';

import { isEmailValid } from '../service_functions/email.js';
import sgMail from '@sendgrid/mail';

console.log("auth route");

const miniApp = express.Router();

const [days, hours, minutes] = [0,2,0];
const jwtConfig = {
  tokenKey,  // fixed tokenKey signature
  expiresIn: days * 24 * 60 * 60 * 1000 + hours * 60 * 60 * 1000 + minutes * 60 * 1000, // days * hours * minutes * seconds * milliseconds
};


//Login
miniApp.post("/login", async (req, res) => {
  console.log("server/auth/login endpoint");

  try {
    const { email, password } = req.body;
    console.log(email+" "+password);

    if (isEmailValid(email)){

      const user = await getUser(req.body); 
      console.log("user : " + user);
   
      if (user !== null && user.email && user.email !== undefined){ // user found
        if (user.isVerified === true) { // user verified

          const isValidPassword = await bcrypt.compare (password, user.password);
          console.log("validity : " + isValidPassword);

          if (user.email === email && isValidPassword) { // right logins
            console.log("right credentials");
            const userData = Object.assign({}, user); // create a copy of user
            delete userData.password;  // Modify in the future to hash password with register then compare with login
            userData.accessToken = jwt.sign( //jwt.sign(payload, secretOrPrivateKey, [options, callback])
              { _id: userData._id }, 
              jwtConfig.tokenKey, 
              { expiresIn: jwtConfig.expiresIn }
              ); // generate jwt token

            return res.status(200).send(userData); 
          } 
          else { // wrong logins
            res.status(400).send("Wrong login and/or password.");
          }
        }
        else { // user not found or database error
          console.log("not lmfao");
          res.status(400).send("Account not verified and activated.");
        };
      }
      else {
        console.log("not lol");
        res.status(400).send("User not found or database error.");
      }
    }
    else {
      res.status(400).send("Wrong email format");
    };
  }
  catch (error){
    console.log(error);
    res.status(400).send(error);
  };
});


//Register
miniApp.post("/register", async (req, res) => {
  console.log("/auth/register endpoint");
  try {
    const { email, password, name } = req.body;

    if (isEmailValid(email)){

      const checkedUser = await UserModel.find({ email: email }); //returns a list of users owning this email
      const isAlreadyRegistered = checkedUser.length > 0;

      if (!isAlreadyRegistered) {

        console.log("password from input : " + password);
        const cookedPassword = await bcrypt.hash(password, 10);
        console.log("stored password cooked : " + cookedPassword);

        const newUser = {
          email,
          password: cookedPassword,
          name,
          avatar: "",
          role: "client",
          isVerified: false
        };
        const userData = await addUser(newUser);

        console.log("back to register endpoint");

        const verificationCode = randomstring.generate({
          length: 50,
          charset: 'alphanumeric'
        });
        console.log("random string for mail : " + verificationCode);
        
        sgMail.setApiKey(sendgridApiKey);
        const text = "If you are registering your new Gasanaly account, let's end it now. Click the following link : http://client.carbonapp.localhost/#/auth/verify-account?verificationCode=" + verificationCode + "&userId="+ userData._id;
        const html = "<p>If you are registering your new Gasanaly account, let's end it now. Click the link down below :</p><br><br><a href='http://client.carbonapp.localhost/#/auth/verify-account?verificationCode=" + verificationCode + "&userId="+ userData._id +"' rel='noreferrer noopener'>Click this confirmation link !</a>";
        console.log("text :\n" + text);
        const msg = {
          to: email, // Change to your recipient
          from: 'rmonje@helius-systems.com', // Change to your verified sender
          subject: 'Gasanaly verification email',
          text: text,
          html: html, //put the right link/endpoint
        }
        sgMail
          .send(msg)
          .then(() => {
            console.log('Email sent to ' + email);
          })
          .catch((error) => {
            console.error(error);
          });
          
        const couple = await addVerificationCouple({"userId": userData._id, verificationCode}); // userId unique which means just one verification email ca be sent to an user

        return res.status(200).send("User registered. Waiting for email validation ...");
      }

      res.status(401).send("This email is already in use.");  
    }
    else {
      res.status(400).send("Wrong email format");
    }
  }
  catch (error){
    console.log(error);
    res.status(400).send(error);
  };
});


//Verify Account
miniApp.post("/verify-account", async (req,res) => { //switch in post method

  console.log("server/auth/verify-account* endpoint");

  try {
    const { verificationCode, userId } = req.body;
    console.log("random string received from mail link : " + verificationCode);
    console.log("userId received from mail link : " + userId);
    
    const couple = await getVerificationCouple({userId});
    console.log("couple : " + couple + " _id = " + couple._id);
    const coupleId = couple._id.toString();
    console.log("id new : ", coupleId);

    if(couple && couple!== null && couple!== undefined){
      console.log("we have a couple");
      console.log(" _id = " + couple._id); // ISSUE HERE
      

      const todayDate = new Date();
      const objectDate = new Date(couple.createdAt);

      const objectLife = todayDate.getMilliseconds() - objectDate.getMilliseconds();
      console.log("object life : ", objectLife, "ms");
      console.log(Math.floor(objectLife/(1000*60**2*24)) + "days " + Math.floor(objectLife/(1000*60**2)) + "hours " + Math.floor(objectLife/(1000*60)) + "minutes " + objectLife/(1000) + "seconds");

      const validityDuration = 1000*( 0/*seconds*/ +60*( 0/*minutes*/ +60*( 6/*hours*/ +24*( 0/*days*/ )))); //in milliseconds

      if (objectLife < validityDuration){
        console.log("activation link still valid");
      
        const isMatching = verificationCode === couple.verificationCode;

        if(isMatching){
          const user = await UserModel.findOneAndUpdate({_id: userId}, {isVerified: true}, {new: true});

          const userData = Object.assign({}, user); // create a copy of user
          delete userData.password;  // Modify in the future to hash password with register then compare with login
          userData.accessToken = jwt.sign( //jwt.sign(payload, secretOrPrivateKey, [options, callback])
            { _id: userData._id }, 
            jwtConfig.tokenKey, 
            { expiresIn: jwtConfig.expiresIn }
          ); // generate jwt token

          await VerificationCoupleModel.deleteOne({ _id: couple._id });

          console.log("User is now verified.");
          res.status(200).send(userData);
        }
        else { // no match
          res.status(400).send("Already activated or wrong activation link");
        };
      }
      else { // activation link not valid anymore
        console.log("delete the invalid couple");
        console.log(" _id = " + coupleId); //IISSSUE WITH THE COUPLE
        await VerificationCoupleModel.deleteOne({_id: coupleId });

        console.log("send a new validation email");
        const newVerificationCode = randomstring.generate({
          length: 50,
          charset: 'alphanumeric'
        });
        console.log("random string for mail : " + newVerificationCode);

        const user = await getUser({"_id": userId});
        console.log("user email : ", user.email);
        
        sgMail.setApiKey(sendgridApiKey);
        const text = "If you are registering your new Gasanaly account, let's end it now. Click the following link : http://client.carbonapp.localhost/#/auth/verify-account?verificationCode=" + newVerificationCode + "&userId="+ userId;
        const html = "<p>If you are registering your new Gasanaly account, let's end it now. Click the link down below :</p><br><br><a href='http://client.carbonapp.localhost/#/auth/verify-account?verificationCode=" + newVerificationCode + "&userId="+ userId +"' rel='noreferrer noopener'>Click this confirmation link !</a>";
        console.log("text :\n" + text);
        const msg = {
          to: user.email, // Change to your recipient
          from: 'rmonje@helius-systems.com', // Change to your verified sender
          subject: 'Gasanaly verification email',
          text: text,
          html: html, //put the right link/endpoint
        }
        sgMail
          .send(msg)
          .then(() => {
            console.log('Email sent to ' + user.email);
          })
          .catch((error) => {
            console.error(error);
          });
          
        const couple = await addVerificationCouple({"userId": userId, "verificationCode": newVerificationCode}); // userId unique which means just one verification email ca be sent to an user


        res.status(500).send("A new validation email has been sent to the mail adress.");
      };
    }
    else { // no couple
      res.status(400).send("Already activated or wrong activation link");
    };
  }
  catch (error) {
    console.log("error");
    res.status(400).send(error);
  };
});


//Account
miniApp.get("/account", auth, async (req, res) => {
  console.log("/auth/account endpoint");
  const accessToken =
  req.body.accessToken || req.query.accessToken || req.header("x-access-token") || req.header("accessToken");

  if (accessToken) {
    const _id = req.body.user._id
    const user = await getUser({"_id": _id});
    const userData = Object.assign({}, user);

    delete userData.password;
    userData.accessToken = jwt.sign({ _id: userData._id }, jwtConfig.tokenKey, {
      expiresIn: jwtConfig.expiresIn,
    }); // refresh jwt token

    return res.status(200).send(userData);
  }

  res.status(401);
});


//Log out
miniApp.get("/logout", auth, (req,res) => {
  console.log("/auth/logout endpoint");
  res.status(200).send("User has logged out.");
});

export default miniApp;