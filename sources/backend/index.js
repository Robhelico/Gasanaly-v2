import http from 'http';
import app from './app.js';
const server = http.createServer(app);
import { port, mongodbUri } from './config/index.js';
import mongoose from 'mongoose';

console.log("index is running");

mongoose.connect(mongodbUri,{ useNewUrlParser: true, useUnifiedTopology: true })
  .then (() => {
    console.log("Connecetd to Mongo database");
    // server listening 
    server.listen(port, () => {
      console.log(`Server running on port ${port}`);
    });
  })
  .catch((error) => {
    console.log("database connection failed.");
    console.log(error);
  });