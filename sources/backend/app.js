import express from 'express';
import auth from './routers/authentication.js';
import test from './routers/test.js';
import public_api from './routers/publicApi.js';

const app = express();
app.use(express.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, accessToken, authorization");
    next();
});

app.use("/auth", auth);
app.use("",test);
app.use("/public_api", public_api);

console.log("app iz lezgooo");

export default app;